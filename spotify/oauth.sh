#!/bin/bash
#gen the URL to open for authentication
client_id='OAUTH_KEY'
port=8082
redirect_uri=http%3A%2F%2Flocalhost%3A$port%2F
scopes="user-read-currently-playing%20user-modify-playback-state"
#open the address for auth in default browser.
xdg-open https://accounts.spotify.com/authorize/?response_type=code\&client_id=$client_id\&redirect_uri=$redirect_uri\&scope=$scopes
# Grab token and save to file
rm -f ./key
response="key"
(echo "HTTP/1.1 <html><body><script>window.close();</script>this should close?</body></html>") | nc -l $port -q 0 > $response
code=$(cat $response | grep GET | cut -d' ' -f 2 | cut -d'=' -f 2)
echo "$code" > "key"
#TODO get browser window to actually close, above script does not work, might need a redirect to an _actual_ HTML payload?
echo $code
